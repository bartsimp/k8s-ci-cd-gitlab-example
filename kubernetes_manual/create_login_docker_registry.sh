#!/usr/bin/env bash

kubectl create -f namespace.yaml

# PRIVATE PROJECT
kubectl -n hellocicd create secret docker-registry regcred --docker-server=https://registry.gitlab.com --docker-username=YOURUSER --docker-password=YOURPASSWORD --docker-email=your@mail.tld