# Hello CI/CD K9s

Example CI/CD project with gitlab and deployment on kubernetes

Example deployed website on

https://hellocicd.nutellino.it 

## Italian Tutorial

https://www.nutellino.it/2018/09/25/gitlab-com-ci-cd-di-un-sito-statico-su-kubernetes/

## Commands

python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' < .kube/config > kubeconfigdemo.json  

---

kubectl create namespace hellocicd

---

kubectl apply -n hellocicd -f serviceLoadBalancer.yaml

kubectl delete -n hellocicd -f serviceLoadBalancer.yaml